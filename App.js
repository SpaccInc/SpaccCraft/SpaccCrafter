function RenderItemList() {
	let ItemTileTemplate = ItemList.innerHTML;
	ItemList.innerHTML = "";
	for (i in Items) {
		ThisItemTile = ItemTileTemplate
			.replace("ItemTileImgId", "ItemTileImg"+i)
			.replace("ItemTileImg.png", i+".png")
			.replace("ItemTileLabelId", "ItemTileLabel"+i)
			.replace("ItemTileLabel.txt", i);
		ItemList.innerHTML += ThisItemTile;
	}
}

RenderItemList();
